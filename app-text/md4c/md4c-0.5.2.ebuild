# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..13} )

inherit cmake python-any-r1

DESCRIPTION="C Markdown parser. Fast. SAX-like interface."
HOMEPAGE="https://github.com/mity/md4c"
SRC_URI="
	https://github.com/mity/md4c/archive/refs/tags/release-${PV}.tar.gz -> ${P}.tar.gz
"
S="${WORKDIR}/md4c-release-${PV}"

LICENSE="MIT CC-BY-SA-4.0"
SLOT="0"
KEYWORDS="~amd64"

IUSE="test"
RESTRICT="!test? ( test )"

BDEPEND="
	test? (
		${PYTHON_DEPS}
	)
"

pkg_setup() {
	use test && python-any-r1_pkg_setup
}

src_test() {
	"${EPYTHON}" "${S}"/test/run-testsuite.py -p "${BUILD_DIR}"/md2html/md2html -s "${S}"/test/spec.txt || die
	"${EPYTHON}" "${S}"/test/pathological-tests.py -p "${BUILD_DIR}"/md2html/md2html || die
}
