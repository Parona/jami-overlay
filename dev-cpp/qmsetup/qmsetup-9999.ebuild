# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="CMake Modules and Basic Libraries for C/C++ projects."
HOMEPAGE="https://github.com/stdware/qmsetup"
if [[ "${PV}" == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://github.com/stdware/qmsetup/"
else
	COMMIT="89fa57046241c26dfcfd97ceba174728b24bdd27"
	SRC_URI="https://github.com/stdware/qmsetup/archive/${COMMIT}.tar.gz -> ${PN}-${COMMIT}.tar.gz"
	S="${WORKDIR}/${PN}-${COMMIT}"
	KEYWORDS="~amd64"
fi

LICENSE="MIT"
SLOT="0"

RESTRICT="test" # no tests

RDEPEND="
	dev-cpp/syscmdline
"
DEPEND="${RDEPEND}"
