# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="A C++ library for NAT traversal and secure communication"
HOMEPAGE="https://git.jami.net/savoirfairelinux/dhtnet"
if [[ "${PV}" == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://git.jami.net/savoirfairelinux/dhtnet"
else
	COMMIT="c5ce26957afb86002a35a18c3faf126b71e605fc"
	SRC_URI="https://git.jami.net/savoirfairelinux/dhtnet/-/archive/${COMMIT}/dhtnet-${COMMIT}.tar.bz2"
	S="${WORKDIR}/${PN}-${COMMIT}"
	KEYWORDS="~amd64"
fi

LICENSE="GPL-3"
SLOT="0"

IUSE="systemd test upnp"
RESTRICT="!test? ( test )" #TODO: wrangle tests, currently hangs
PROPERTIES="test_network"
# natpmp
CDEPEND="
	>=dev-cpp/msgpack-cxx-5.0.0
	dev-cpp/opendht:=
	dev-cpp/yaml-cpp:=
	dev-libs/libfmt:=
	dev-libs/openssl:=
	sys-libs/readline:=
	net-libs/gnutls:=
	net-libs/pjproject-jami:=
	upnp? (
		net-libs/libupnp:=
	)
"
RDEPEND="
	${CDEPEND}
"
DEPEND="
	${CDEPEND}
	test? (
		dev-util/cppunit
	)
"

BDEPEND="
	virtual/pkgconfig
"

PATCHES=(
	"${FILESDIR}"/dhtnet-0.0.1_p20240805-libfmt-11.patch
)

CMAKE_SKIP_TESTS=(
	# Timeout
	tests_connectionManager
	tests_ice
	tests_peerDiscovery
)

src_configure() {
	local mycmakeargs=(
		-DBUILD_BENCHMARKS=OFF
		-DBUILD_DEPENDENCIES=OFF
		-DBUILD_TESTING=$(usex test)
		-DBUILD_TOOLS=ON
		-DDHTNET_NATPMP=OFF
		-DDHTNET_PUPNP=$(usex upnp)
		-DDHTNET_TESTABLE=$(usex test)
		-DDNC_SYSTEMD=$(usex systemd)
	)
	cmake_src_configure
}
