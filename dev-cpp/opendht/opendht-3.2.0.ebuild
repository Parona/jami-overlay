# Copyright 2022-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="C++17 Distributed Hash Table implementation"
HOMEPAGE="https://github.com/savoirfairelinux/opendht"
SRC_URI="https://github.com/savoirfairelinux/opendht/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3+"
SLOT="0/$(ver_cut 1-2)"
KEYWORDS="~amd64"

IUSE="doc systemd test tools"
RESTRICT="!test? ( test )"

DEPEND="
	app-crypt/argon2:=
	dev-cpp/asio
	dev-cpp/expected-lite
	>=dev-cpp/restinio-0.7
	dev-libs/jsoncpp:=
	dev-libs/libfmt:=
	dev-libs/openssl:=
	dev-cpp/msgpack-cxx
	dev-libs/nettle:=
	net-libs/gnutls:=
	net-libs/llhttp
	sys-libs/liburing:=
	tools? (
		sys-libs/readline
	)
"
RDEPEND="${DEPEND}"
BDEPEND="
	doc? (
		app-text/doxygen
	)
"

src_prepare() {
	sed -i -e "/DESTINATION share\/doc/ s/${PN}/${P}/" doc/CMakeLists.txt || die
	sed -ie -e 's/libllhttp/libllhttp.so/' CMakeLists.txt || die
	cmake_src_prepare
}

src_configure() {
	local mycmakeargs=(
		-DBUILD_TESTING=$(usex test)
		-DOPENDHT_C=ON
		-DOPENDHT_DOCUMENTATION=$(usex doc)
		-DOPENDHT_HTTP=ON
		-DOPENDHT_INDEX=ON
		-DOPENDHT_PEER_DISCOVERY=ON
		-DOPENDHT_PROXY_CLIENT=ON
		-DOPENDHT_PROXY_OPENSSL=ON
		-DOPENDHT_PROXY_SERVER=ON
		-DOPENDHT_PROXY_SERVER_IDENTITY=ON
		-DOPENDHT_PUSH_NOTIFICATIONS=ON
		-DOPENDHT_PYTHON=OFF
		-DOPENDHT_SANITIZE=OFF
		-DOPENDHT_STATIC=OFF
		-DOPENDHT_SYSTEMD=$(usex systemd)
		-DOPENDHT_TESTS_NETWORK=OFF
		-DOPENDHT_TOOLS=$(usex tools)
	)
	cmake_src_configure
}
