# Copyright 2022-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="Asynchronous HTTP/WebSocket server C++ library"
HOMEPAGE="https://github.com/Stiffstream/restinio"
SRC_URI="
	https://github.com/Stiffstream/restinio/archive/refs/tags/v.${PV}.tar.gz -> ${P}.tar.gz
"
S="${WORKDIR}/${PN}-v.${PV}"
LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

IUSE="test"
RESTRICT="!test? ( test )"

CDEPEND="
	dev-cpp/asio
	dev-cpp/expected-lite
	>=dev-libs/boost-1.66
	dev-libs/libfmt
	dev-libs/libpcre2
	dev-libs/libpcre
	dev-libs/openssl
	net-libs/llhttp
	sys-libs/zlib
"
DEPEND="
	${CDEPEND}
	test? ( dev-cpp/catch:0 )
"
RDEPEND="${CDEPEND}"

CMAKE_USE_DIR="${S}/dev"

PATCHES=(
	"${FILESDIR}"/restinio-0.7.2-libfmt-11.patch
)

src_configure() {
	local mycmakeargs=(
		-DRESTINIO_BENCHMARK=OFF
		-DRESTINIO_DEP_CATCH2=find
		-DRESTINIO_DEP_EXPECTED_LITE=system
		-DRESTINIO_DEP_FMT=system
		-DRESTINIO_DEP_LLHTTP=system
		-DRESTINIO_INSTALL=ON
		-DRESTINIO_SAMPLE=OFF
		-DRESTINIO_TEST=$(usex test)
		-DRESTINIO_WITH_SOBJECTIZER=OFF
	)
	cmake_src_configure
}

src_test() {
	# Tests use a single port for their server and will fail if run in parallel
	MAKEOPTS="-j1" cmake_src_test -j1
}
