# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="C++ Advanced Command Line Parser"
HOMEPAGE="https://github.com/SineStriker/syscmdline"
SRC_URI="https://github.com/SineStriker/syscmdline/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

IUSE="test"
RESTRICT="!test? ( test )"

src_configure() {
	local mycmakeargs=(
		-DSYSCMDLINE_BUILD_STATIC=OFF
		-DSYSCMDLINE_BUILD_TESTS=$(usex test)
	)
	cmake_src_configure
}

src_test() {
	"${BUILD_DIR}"/bin/tst_basic || die
}
