# Copyright 2022-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10..13} )

QTMIN="6.6"

inherit cmake flag-o-matic python-any-r1 virtualx xdg

DESCRIPTION="GNU Jami client made with QT"
HOMEPAGE="https://git.jami.net/savoirfairelinux/jami-client-qt"

if [[ "${PV}" == "99999999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://git.jami.net/savoirfairelinux/jami-client-qt.git"
	EGIT_SUBMODULES=( '*' '-daemon' '-3rdparty/libqrencode' '-3rdparty/qrencode-win32' )

	SLOT="0"
else
	declare -A THIRDPARTY=(
		[SortFilterProxyModel]="a2a0c72e4db38e1c5478bd3e0f67ff99fae11f00"
		[md4c]="ad8d41127b94e2f0633ad14b3787f0bc4613a689" # can be unbundled
		[tidy-html5]="d08ddc2860aa95ba8e301343a30837f157977cba"
	)

	TYPE="stable"

	SRC_URI="
		https://git.jami.net/savoirfairelinux/jami-client-qt/-/archive/${TYPE}/${PV}/jami-client-qt-${TYPE}-${PV}.tar.bz2
		https://github.com/atraczyk/SortFilterProxyModel/archive/${THIRDPARTY[SortFilterProxyModel]}.tar.gz
			-> SortFilterProxyModel-${THIRDPARTY[SortFilterProxyModel]}.tar.gz
		https://github.com/mity/md4c/archive/${THIRDPARTY[md4c]}.tar.gz
			-> md4c-${THIRDPARTY[md4c]}.tar.gz
		https://github.com/htacg/tidy-html5/archive/${THIRDPARTY[tidy-html5]}.tar.gz
			-> tidy-html5-${THIRDPARTY[tidy-html5]}.tar.gz
	"
	S="${WORKDIR}/jami-client-qt-${TYPE}-${PV}"
	KEYWORDS="~amd64"
	SLOT="0/${TYPE}"
fi

LICENSE="GPL-3+"

IUSE="doc test +webengine"
RESTRICT="test !test? ( test )" # wrangle tests

# plenty automagic to deal with
RDEPEND="
	dev-libs/glib:2
	>=dev-qt/qt5compat-${QTMIN}:6
	>=dev-qt/qtbase-${QTMIN}:6[concurrent,dbus,gui,sql,sqlite,network,widgets]
	>=dev-qt/qtdeclarative-${QTMIN}:6
	>=dev-qt/qtmultimedia-${QTMIN}:6[gstreamer]
	>=dev-qt/qtpositioning-${QTMIN}:6
	>=dev-qt/qtsvg-${QTMIN}:6
	gui-libs/qwindowkit
	media-gfx/qrencode:=
	media-libs/vulkan-loader
	media-video/ffmpeg:=
	net-libs/jami-daemon[plugins(+),upnp]
	net-misc/networkmanager
	>=x11-libs/gdk-pixbuf-2.40.0:2
	x11-libs/libnotify
	x11-libs/libX11
	x11-libs/libxcb:=
	webengine? (
		>=dev-qt/qtwebengine-${QTMIN}:6[qml,widgets]
		>=dev-qt/qtwebchannel-${QTMIN}:6[qml]
	)
"
DEPEND="
	${RDEPEND}
	x11-base/xorg-proto
	test? (
		dev-cpp/gtest
		>=dev-qt/qthttpserver-${QTMIN}:6
	)
"
BDEPEND="
	${PYTHON_DEPS}
	virtual/pkgconfig
	doc? (
		app-text/doxygen[dot]
	)
"

src_prepare() {
	if ! [[ ${PV} =~ *9999* ]]; then
		for name in "${!THIRDPARTY[@]}"; do
			mv -T "${WORKDIR}/${name}-${THIRDPARTY[${name}]}" "${S}/3rdparty/${name}" || die
		done
	fi

	# unbundle QWindowKit
	sed -i \
		-e '/add_fetch_content(/,/^)$/ s/^/#/' \
		-e '/# qwindowkit (frameless window)/afind_package(QWindowKit CONFIG REQUIRED)' \
		-e '/${QWindowKit_BINARY_DIR}/iget_target_property(QWINDOWKIT_INCLUDES QWindowKit::Quick INTERFACE_INCLUDE_DIRECTORIES)' \
		-e 's|${QWindowKit_BINARY_DIR}/include|${QWINDOWKIT_INCLUDES}|' \
		CMakeLists.txt

	cmake_src_prepare
}

src_configure() {
	# lets be super safe
	strip-flags
	filter-lto

	# Push to talk depends on X11 being the XDG env...
	xdg_environment_reset

	export XDG_SESSION_TYPE=x11

	local mycmakeargs=(
		$(cmake_use_find_package doc Doxygen)
		-DWITH_WEBENGINE=$(usex webengine ON OFF) # it matters
		-DBUILD_TESTING="$(usex test)"
		-DENABLE_LIBWRAP=ON
		-DJAMICORE_AS_SUBDIR=OFF
		-DWITH_DAEMON_SUBMODULE=OFF
		-DPython3_EXECUTABLE=${EPYTHON}
	)
	cmake_src_configure
}

src_test() {
	unset WAYLAND_DISPLAY
	mytest() {
		local -a failed=()
		for test in {unit_tests,qml_tests}; do
			set -- "${BUILD_DIR}"/tests/${test}
			einfo "${@}"
			"${@}"
			if [[ $? != 0 ]]; then
				failed+=( ${test} )
			fi
		done
		[[ ${#failed[@]} > 0 ]] && die "Some tests failed: ${failed[@]}"
	}

	virtx mytest
}
