# TODO

- fix tests
    * jami 8 failed tests
    * jami-daemon
    ```
    8/41 conference                 FAIL            764.79s   exit status 1
    10/41 conversation_call          TIMEOUT        1800.03s   killed by signal 15 SIGTERM
    12/41 conversation_members_event FAIL           1126.61s   killed by signal 11 SIGSEGV
    18/41 ice                        FAIL             92.88s   exit status 1
    26/41 media_negotiation          FAIL            200.30s   exit status 1
    28/41 recorder                   FAIL            198.29s   exit status 1
    29/41 resampler                  FAIL              0.54s   exit status 1
    39/41 plugins                    FAIL            201.18s   exit status 1
    ```
    * dhtnet 1 failed test
        - tests_ice

## BUGS
- white webcam output
    * missing codecs in pjproject?
- syncronization after invitation that never happens
    * upnp support?
