# Copyright 2022-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit meson xdg-utils virtualx

DESCRIPTION="Daemon application for GNU Jami"
HOMEPAGE="https://git.jami.net/savoirfairelinux/jami-daemon"

if [[  "${PV}" == "9999" ]]; then
	inherit git-r3
	EGIT_REPO_URI="https://git.jami.net/savoirfairelinux/jami-daemon"
else
	COMMIT="96684fb5de8edb7d90086f03920429c2207b8a60"
	SRC_URI="
		https://git.jami.net/savoirfairelinux/jami-daemon/-/archive/${COMMIT}/jami-daemon-${COMMIT}.tar.gz
	"
	S="${WORKDIR}/${PN}-${COMMIT}"
	KEYWORDS="~amd64"
fi

LICENSE="GPL-3+"
SLOT="0"

IUSE="alsa dbus jack nodejs portaudio +plugins pulseaudio test upnp"
RESTRICT="test !test? ( test )" # wrangle tests
REQUIRED_USE="test? ( plugins )"

COMMON_DEPEND="
	dev-cpp/dhtnet[upnp?]
	>=dev-cpp/opendht-2.1.0:=
	>=dev-cpp/yaml-cpp-0.5.1:=
	>=dev-libs/jsoncpp-1.6.5:=
	>=dev-libs/libfmt-5.3:=
	>=dev-libs/libgit2-1.1.0:=
	>=dev-libs/libsecp256k1-0.1:=
	dev-libs/nettle:=
	dev-libs/openssl:=
	media-libs/speex
	media-libs/speexdsp
	media-video/ffmpeg:=
	media-libs/webrtc-audio-processing:0
	>=net-libs/gnutls-3.6.7:=
	net-libs/pjproject-jami:=[alsa?,epoll,ffmpeg,portaudio?,speex,webrtc]
	sys-libs/zlib
	virtual/libudev:=
	alsa? (
		media-libs/alsa-lib
	)
	dbus? (
		dev-cpp/sdbus-c++:=
	)
	jack? (
		virtual/jack
	)
	portaudio? (
		media-libs/portaudio
	)
	plugins? (
		>=app-arch/libarchive-3.4.0:=
	)
	pulseaudio? (
		media-libs/libpulse
	)
"
DEPEND="
	test? (
		>=dev-util/cppunit-1.12
	)
"
RDEPEND="${COMMON_DEPEND}"
BDEPEND="
	dev-lang/perl
	dbus? (
		dev-cpp/sdbus-c++[tools]
	)
	nodejs? (
		dev-lang/swig
		net-libs/nodejs
	)
"

PATCHES=(
	"${FILESDIR}"/jami-daemon-15.2.0_p20240305-missing-limits-header.patch
)

src_configure() {
	local interfaces=(
		library
		$(usev dbus)
		$(usev nodejs)
	)
	local meson_interfaces=$(IFS=','; echo "${interfaces[*]}")
	local emesonargs=(
		$(meson_feature alsa)
		$(meson_feature jack)
		$(meson_feature portaudio)
		$(meson_feature pulseaudio)
		$(meson_feature upnp)
		$(meson_use plugins)
		$(meson_use test tests)
		-Dhw_acceleration=true
		-Dinterfaces=${meson_interfaces}
		-Dname_service=enabled
		-Dopensl=disabled
		-Dspeex_ap=enabled
		-Dvideo=true
		-Dwebrtc_ap=enabled
	)
	meson_src_configure
}

src_test() {
	xdg_environment_reset

	# Summary of Failures:
	# 1/39 account_archive            FAIL            481.71s   exit status 1
	# 2/39 account_factory            FAIL             60.88s   exit status 1
	# 4/39 auto_answer                FAIL            121.77s   exit status 1
	# 6/39 call                       FAIL            541.80s   exit status 1
	# 7/39 conference                 TIMEOUT        1800.06s   killed by signal 15 SIGTERM
	# 8/39 conversation               FAIL           1535.88s   exit status 1
	# 9/39 conversation_call          FAIL            451.09s   exit status 1
	# 10/39 conversation_fetch_sent    FAIL             90.85s   exit status 1
	# 11/39 conversation_members_event FAIL           1082.21s   exit status 1
	# 12/39 conversation_repository    FAIL            420.93s   exit status 1
	# 13/39 conversation_request       FAIL            781.08s   exit status 1
	# 14/39 file_transfer              FAIL            661.72s   exit status 1
	# 16/39 hold_resume                FAIL             61.76s   exit status 1
	# 17/39 ice_sdp_parser             TIMEOUT        1800.02s   killed by signal 15 SIGTERM
	# 24/39 media_negotiation          FAIL            181.14s   exit status 1
	# 25/39 migration                  FAIL             71.79s   exit status 1
	# 26/39 recorder                   FAIL            301.56s   exit status 1
	# 27/39 resampler                  FAIL              0.56s   exit status 1
	# 28/39 revoke                     FAIL            121.95s   exit status 1
	# 30/39 sip_basic_calls            TIMEOUT        1800.03s   killed by signal 15 SIGTERM
	# 31/39 presence                   FAIL            152.05s   exit status 1
	# 32/39 typers                     FAIL            121.96s   exit status 1
	# 33/39 sip_srtp                   TIMEOUT        1800.03s   killed by signal 15 SIGTERM
	# 35/39 sync_history               FAIL            841.34s   exit status 1
	# 37/39 plugins                    FAIL            601.62s   exit status 1

	local -x XDG_CONFIG_HOME="${T}/jami-unittest"
	local -x XDG_DATA_HOME="${T}/jami-unittest"
	local -x XDG_CACHE_HOME="${T}/jami-unittest"
	local -x SIPLOGLEVEL=5

	virtx meson_src_test
}
