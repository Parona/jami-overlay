# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

# To make the node-modules tarball:
# PV=
# git fetch
# git checkout v$PV
# npm ci --include=dev
# XZ_OPT=-9 tar --xform="s:^:llhttp-release-v$PV/:" -Jcf llhttp-node-modules-$PV.tar.xz node_modules

DESCRIPTION="Port of http_parser to llparse"
HOMEPAGE="https://llhttp.org/"

# tags are used on purpose
SRC_URI="
	https://github.com/nodejs/llhttp/archive/refs/tags/release/v${PV}.tar.gz -> ${PN}-release-${PV}.tar.gz
	test? (
		https://github.com/nodejs/llhttp/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz
		https://gitlab.com/api/v4/projects/34422305/packages/generic/llhttp/${PV}/llhttp-node-modules-${PV}.tar.xz
	)
"
S="${WORKDIR}/llhttp-release-v${PV}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"

IUSE="test"
RESTRICT="!test? ( test )"

BDEPEND="
	test? (
		net-libs/nodejs[npm]
	)
"

src_test() {
	pushd "${WORKDIR}/${P}" > /dev/null || die
	npm test || die
	popd > /dev/null || die
}
