# Copyright 2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake

DESCRIPTION="Cross-platform frameless window framework for Qt"
HOMEPAGE="https://github.com/stdware/qwindowkit"
SRC_URI="
	https://github.com/stdware/qwindowkit/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz
"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	dev-cpp/qmsetup
	dev-qt/qtbase:6[gui,widgets]
	dev-qt/qtdeclarative:6
"
DEPEND="
	${RDEPEND}
"

src_prepare() {
	cmake_src_prepare

	# Force Qt6
	# https://github.com/stdware/qwindowkit/issues/39
	sed -i -e 's/Qt6 Qt5/Qt6/' src/QWindowKitConfig.cmake.in || die
}

src_configure() {
	local mycmakeargs=(
		-DQWINDOWKIT_BUILD_STATIC=OFF
		-DQWINDOWKIT_BUILD_WIDGETS=ON
		-DQWINDOWKIT_BUILD_QUICK=ON
		-DQWINDOWKIT_BUILD_EXAMPLES=OFF
		-DQWINDOWKIT_BUILD_DOCUMENTATIONS=OFF
		-DQMSETUP_FIND_QT_ORDER=Qt6
	)
	cmake_src_configure
}
