# How to use the overlay

## Enable qt and this overlay
```
# eselect repository add jami-overlay git https://gitlab.com/Parona/jami-overlay
```

## Sync the overlays
```
# emaint sync -r jami-overlay
```
